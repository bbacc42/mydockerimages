-- this creates the database with initial data
--
create database phone;
  use phone;
  create table numbers (
      name varchar(50),
      number varchar(30)
  );


insert into numbers values('Steve', '555-123');
insert into numbers values('Will', '555-124');
insert into numbers values('Augie', '555-125');
